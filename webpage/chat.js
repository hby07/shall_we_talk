// 页面加载时检查状态
var socket;
window.onload = function () {
    // 连接ws
    socket = new WebSocket('ws://127.0.0.1:8888/chat');
    socket.addEventListener('open', function (event) {
        // window.alert("连接成功");
        SetStatus("在线");
        // OnlineList();
    });
    socket.addEventListener('message', function (event) {
        // window.alert('Message from server ', event.data);
        ReceiveMessage(event.data)
    });
    socket.addEventListener('close', function (event) {
        // window.alert('连接已被关闭 ', event.data);
        SetStatus("离线");
    });
}

function Tips() {
    window.alert("欢迎你");
}
// 当前状态
function SetStatus(status) {
    document.getElementById("status").innerText = status;
}

// 在线列表
function OnlineList() {
    let ul = document.getElementById("online");
    for (let index = 0; index < 10; index++) {
        let li = document.createElement("li");
        li.innerText = "张三" + index;
        ul.appendChild(li);
    }
}

// 查看聊天记录
let btn = document.getElementById("record");
btn.onclick = function () {
    window.alert("功能开发中");
}

// 断开连接
let closeBtn = document.getElementById("close");
closeBtn.onclick = function () {
    socket.close(1000, "再见");
}

// 发送消息
let sendBtn = document.getElementById("send");
sendBtn.onclick = function () {
    let oSend = document.getElementById("input-content");
    SendMessage(oSend.innerText);
    // 发送消息后清空输入框
    oSend.innerText = "";

}
// 发送
function SendMessage(message) {
    // window.alert("消息内容:" + message)
    socket.send(message);
    AddToChatArea(message, "sender");
}
// 将内容添加到聊天区
function AddToChatArea(message, SorR) {
    let bubble = document.createElement("div");
    bubble.style.width = "60%";
    bubble.style.marginBottom = "20px";
    bubble.style.position = "relative";
    // bubble.style.backgroundColor = "violet";
    switch (SorR) {
        case "sender":
            bubble.style.float = "right";
            bubble.id = "bubble-self";
            bubble.style.right = "10px";
            break;
        case "receiver":
            bubble.style.float = "left";
            bubble.id = "bubble-other";
            bubble.style.left = "10px";
            break;
        default:
            break;
    }
    bubble.innerText = message;
    document.getElementById("chat").appendChild(bubble);

}

// 接收消息
function ReceiveMessage(message) {
    // message = "服务器发来一条消息";
    AddToChatArea(message, "receiver");
}

// 监听组合键ctrl+回车.发送消息
document.onkeydown = function (e) {
    var key = e.key;
    var ctrlKey = e.ctrlKey;
    if (ctrlKey && key == "Enter") {
        let oSend = document.getElementById("input-content");
        SendMessage(oSend.innerText);
        // 发送消息后清空输入框
        oSend.innerText = "";
    }
}